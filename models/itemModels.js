
const mongoose = require('mongoose');
const {ObjectId} = mongoose.Schema.Types;
const itemSchema = new mongoose.Schema({
  itemName: String,
  itemImages: [String],
  itemDescription: String,
  price: Number,
  location:String,
  status: {
    type:String,
    enum:['Sold','Un-Sold'],
    default:'Un-Sold',
  },
  seller:{
    type:ObjectId,
    ref:"User",
    required:true,
  },
});

module.exports = mongoose.model('Item', itemSchema);

