

const fbForm = document.getElementById("fb");

const postFeedBack = (e) =>{
    e.preventDefault();
    const feedback = document.getElementById('feedBack').value;
    const data = JSON.parse(localStorage.getItem('data'));
    const userName = data.data.user.name;
    const email = data.data.user.email;

    const dat = {
        userName,
        email,
        feedback,
    }

    fetch("/users/fb",{
        method:"POST",
        headers:{"Content-Type":"application/json"},
        body:JSON.stringify(dat)
    }).then((res) =>{
        return res.json();
    }).then((data) =>{
        console.log(data)
        if(data.success){
            alert("Submitted Your feedback successfully");
            // window.location.href = "fb.html"
        }
    })
}
fbForm.addEventListener('submit',postFeedBack);