

const getAllUsers = () =>{
    fetch("/users")
    .then((res) =>res.text())
    .then((data) =>{
        console.log(data)
        const d = JSON.parse(data);
        showUsers(d.data)
    }).catch((err) =>{
        console.log(err);
        alert(err.message);
    })
}

getAllUsers()

const showUsers = (user) =>{
    user.forEach((user) =>{
        if(user.role === 'user'){
            showUser(user);
        }
    })
}

function showUser(user){
    console.log(user,"infor")
    var table = document.getElementById("myTable")
    var row = table.insertRow(table.length)
    var td =  []
    for (i = 0; i < table.rows[0].cells.length;i++){
        td[i] = row.insertCell(i)
    }
    td[0].innerHTML = user.name;
    td[1].innerHTML = user.number;
    td[2].innerHTML = user.email;
    td[3].innerHTML = '<input type="button" onclick="deleteEnrolled(this)" value="Delete" id="button-1"/>'
    td[4].innerHTML = user._id;
    td[4].style.display = 'none';
}


function deleteEnrolled(r) {
    selectedRow = r.parentElement.parentElement
    const id = selectedRow.cells[4].innerHTML
    // cid = selectedRow.cells[1].innerHTML
    const Name = selectedRow.cells[0].innerHTML

    if (confirm(`Are you sure you want to delete ${Name}` )){
        fetch("/users/"+id,{
            method:"DELETE",
            headers:{"Content-type":"application/json; charset=UTF-8"}
        }).then(response =>{
            if (response.ok){
                var rowIndex = selectedRow.rowIndex
                if (rowIndex > 0){
                    selectedRow.parentElement.deleteRow(selectedRow.rowIndex)
                }
                selectedRow = null
            }else{
                throw new Error(response.statusText)
            }
        }).catch(e =>{
            alert(e)
        })
    }
}