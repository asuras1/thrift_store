
const getFeedbacks = () =>{
    console.log("getting feedback")
    fetch("/users/fb")
    .then((res) => res.text())
    .then((data) =>{
        const res = JSON.parse(data);
        if(res.status === 'success'){
            showFeedbacks(res)
        }
    })
}

getFeedbacks();

const showFeedbacks = (res) =>{
    const feedbacks = res.feedback
    feedbacks.forEach(feedback => {
        showFeedback(feedback);
    });
}

const showFeedback = (feedback) =>{
    var table = document.getElementById("myTable")
    var row = table.insertRow(table.length)
    var td =  []
    for (i = 0; i < table.rows[0].cells.length;i++){
        td[i] = row.insertCell(i)
    }
    td[0].innerHTML = feedback.userName;
    td[1].innerHTML = feedback.email;
    td[2].innerHTML = feedback.feedback;
}