window.onload = function(){
    const id = JSON.parse(localStorage.getItem('data')).data.user._id;
    fetch("/items/user/"+id)
    .then(res => res.text())
    .then(data => showItems(data))
    .catch(e =>{
        alert(e)
        console.log(e)
    })
}

function shwoDatum(data) {
    let info = JSON.parse(data)
    newRow(info)
}
function showItems(data) {
    let enrolled = JSON.parse(data)
    const items = enrolled.items;
    console.log(items)
    items.forEach(enrol => {
        newRow(enrol)
    });
}

function newRow(info) {
    console.log(info,"infor")
    var table = document.getElementById("myTable")
    var row = table.insertRow(table.length)
    var td =  []
    for (i = 0; i < table.rows[0].cells.length+1;i++){
        td[i] = row.insertCell(i)
    }
    td[0].innerHTML = info.itemName;
    td[1].innerHTML = info.price;
    td[2].innerHTML = info.location;
    td[3].innerHTML = info.itemDescription;
    td[4].innerHTML = '<input type="button" onclick="deleteEnrolled(this)" value="Delete" id="button-1"/>'
    td[5].innerHTML = '<input type="button" onclick="openPopup(this)" value="Update" id="button-2"/>'
    td[6].innerHTML = info._id;
    td[6].style.display = 'none';

    td[7].innerHTML = info.status;
}

const handleChange = (r)=>{
    selectedRow = r.parentElement.parentElement
    const id = selectedRow.cells[6].innerHTML
    // cid = selectedRow.cells[1].innerHTML
    console.log(id)
    fetch("/users/status/"+id,{
        method:"PATCH",
        headers:{"Content-Type":"application/json"},
    })
    .then((res) => res.text())
    .then((res2) =>{
        console.log(res2);
        const r = JSON.parse(res2)

        const elem = document.getElementById("status");
        console.log(elem.selectedIndex);
        console.log(r.item.status == 'Sold')
        if(r.item.status == 'Sold'){
            elem.selectedIndex = 1
            console.log(1)
        }else{
            elem.selectedIndex = 0
        }
    })
}
// Get the dropdown element


// -------------------------------------------------------

function deleteEnrolled(r) {
    selectedRow = r.parentElement.parentElement
    const id = selectedRow.cells[6].innerHTML
    // cid = selectedRow.cells[1].innerHTML
    const itemName = selectedRow.cells[0].innerHTML

    if (confirm(`Are you sure you want to delete ${itemName}` )){
        fetch("/items/"+id,{
            method:"DELETE",
            headers:{"Content-type":"application/json; charset=UTF-8"}
        }).then(response =>{
            if (response.ok){
                var rowIndex = selectedRow.rowIndex
                if (rowIndex > 0){
                    selectedRow.parentElement.deleteRow(selectedRow.rowIndex)
                }
                selectedRow = null
            }else{
                throw new Error(response.statusText)
            }
        }).catch(e =>{
            alert(e)
        })
    }
}

function updateEnrolled(e) {
    e.preventDefault()
    console.log('hiiii')
    const id = document.getElementById('id').innerHTML;
    const price = document.getElementById('price').value;
    const itemDescription = document.getElementById('productDescription').value;
    const location = document.getElementById('location').value;
    const status = document.getElementById('status').value;
    // cid = selectedRow.cells[1].innerHTML
    const itemName = document.getElementById('productName').value;
    const formData = new FormData();
    if(itemName){
        formData.append('itemName',itemName)
    }
    if(price){
        formData.append('price',price)
    }
    if(itemDescription){
        formData.append('itemDescription',itemDescription)
    }
    if(location){
        formData.append('location',location)
    }
    if(status){
        formData.append('status', status)
    }
    const imageInput = document.getElementById("images");

  for (let i = 0; i < imageInput.files.length; i++) {
    formData.append("itemImages", imageInput.files[i]);
  }
    const dataE = {
        itemName,
        itemDescription,
        price

    }
    console.log(dataE)
    if (confirm(`Are you sure you want to update ${itemName}` )){
        document.getElementById('productName').value = '';
        document.getElementById("price").value = '';
        document.getElementById('location').value = '';
        document.getElementById('productDescription').value = '';
        fetch("/items/"+id,{
            method:"PATCH",
            body:formData,
        }).then(response =>{
            if (response.ok){
                // var rowIndex = selectedRow.rowIndex
                // if (rowIndex > 0){
                //     selectedRow.parentElement.deleteRow(selectedRow.rowIndex)
                // }
                window.location.href = '../user/product.html'
                selectedRow = null
            }else{
                throw new Error(response.statusText)
            }
        }).catch(e =>{
            alert(e)
        })
    }
}

const item = document.getElementById('item')

item.addEventListener('submit',updateEnrolled)