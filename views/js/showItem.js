
window.onload = () =>{

    fetch("/items")
    .then((res) =>res.text())
    .then((res2) => showItems(res2))
    .catch((err) =>{
        console.log(err)
        alert(err.message)
    })
}

const showItems = (items) =>{
    console.log(items)
    const items2 = JSON.parse(items);
    items2.forEach(item => {
        showItem(item)
    });
}

const showItem = (item) =>{
    const div = document.getElementById("add");

    const div2 = document.createElement("div");
    div2.setAttribute("class","product-card");

    const div3 = document.createElement("div");
    div3.setAttribute("class","product-image");

    const img = document.createElement("img");
    img.setAttribute("src","../../"+item.itemImages[0]);
    div3.appendChild(img);

    const p2 = document.createElement('p');
    p2.innerHTML = item._id;
    p2.id='itemId';
    p2.style.display = 'none'

    div2.appendChild(p2);
    div2.setAttribute('onClick','getItem(this)')
    div2.appendChild(div3);

    const div4 = document.createElement("div");
    div4.setAttribute("class","product-details");

    const h1 = document.createElement("h1");
    h1.innerHTML = item.itemName;

    const span = document.createElement("span");
    span.setAttribute("class","price");
    span.innerHTML = ("Nu." + item.price);

    const p = document.createElement("p");
    p.innerHTML = ("Location: " + item.location);
    p.setAttribute("class","p1");


    const button = document.createElement("button");
    button.setAttribute("class","btn");
    button.setAttribute("id",item._id);
    button.style.display='none'

    div4.appendChild(h1);
    div4.appendChild(span);
    div4.appendChild(p);
    div4.appendChild(button);
    div2.appendChild(div4);
    div.appendChild(div2);
    
}

const getItem = (r) =>{
    var p = r.querySelector('p');
    var id = p.innerHTML;

    localStorage.setItem('itemId',id)
    window.location.href = '../home page/content.html'
}