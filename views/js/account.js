


const profileForm = document.getElementById("accountForm");

const updateProfile = (e) =>{
    e.preventDefault();

    const formData = new FormData();
    const name = document.getElementById('username').value;
    const oldPassword = document.getElementById('oldpassword').value;
    const newPassword = document.getElementById('newpassword').value;
    const confirmPassword = document.getElementById('confirmpassword').value;
    if(name){
        formData.append('name',document.getElementById('username').value);
    }
    if(oldPassword){
        formData.append('oldPassword',document.getElementById('oldpassword').value);
    }
    if(newPassword){
        formData.append('newPassword',document.getElementById('newpassword').value);
    }
    if(confirmPassword){
        formData.append('confirmPassword',document.getElementById('confirmpassword').value);
    }
  
    formData.append('profilePhoto',document.getElementById('profile-picture-upload').files[0]);
    const data = JSON.parse(localStorage.getItem('data'))
    const id = data.data.user._id;
    
    fetch('/users/'+id,{
        method:"PATCH",
        body:formData,
    }).then((res) => res.text())
    .then((res2) =>{
        const d = JSON.parse(res2);
        if(d.status === 'success'){
            console.log(d.data)
            const data = JSON.parse(localStorage.getItem('data'));
            data.data.user = d.data;
            localStorage.setItem('data',JSON.stringify(data))
            //alert("Updated successfully")
        }

    })
}

function fetche(){
    const data = JSON.parse(localStorage.getItem('data'))
    const user = data.data.user;
    info(user)

    
}

fetche();

function info(user){
    box1 = document.getElementById('box1')

    name_ = document.createElement("p")
    email = document.createElement("p")

    document.getElementById('profile-image').src = "../../"+ user.profilePhoto;

    name_.innerHTML = user.name;
    email.innerHTML = user.email;

    box1.appendChild(name_)
    box1.appendChild(email)

}

accountForm.addEventListener('submit',updateProfile)

document.getElementById('fileBtn').addEventListener('click', function() {
    document.getElementById('profile-picture-upload').click();
  });
