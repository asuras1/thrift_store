const form = document.querySelector("form");
const eField = form.querySelector(".email");
const eInput = eField.querySelector("input");
const pField = form.querySelector(".password");
const pInput = pField.querySelector("input");
const nField = form.querySelector(".name");
const nInput = nField.querySelector("input");
const cpField = form.querySelector(".confirm-password");
const cpInput = cpField.querySelector("input");

form.onsubmit = (e) => {
  e.preventDefault(); // Prevent form submission

  // Check if the fields are blank and add the "shake" class if they are
  (eInput.value == "") ? eField.classList.add("shake", "error") : checkEmail();
  // (pInput.value == "") ? pField.classList.add("shake", "error") : checkPass();
  (nInput.value == "") ? nField.classList.add("shake", "error") : checkName();
  // (phInput.value == "") ? phField.classList.add("shake", "error") : checkPhone();
  (cpInput.value == "") ? cpField.classList.add("shake", "error") : checkConfirmPassword();

  // Remove the "shake" class after 500ms
  setTimeout(() => {
    eField.classList.remove("shake");
    pField.classList.remove("shake");
    nField.classList.remove("shake");
    // phField.classList.remove("shake");
    cpField.classList.remove("shake");
  }, 500);

  // Check email on keyup
  eInput.onkeyup = () => {
    checkEmail();
  };

  // Check password on keyup
  pInput.onkeyup = () => {
    checkPass();
  };

  // Check name on keyup
  nInput.onkeyup = () => {
    checkName();
  };

  // Check phone number on keyup
  // phInput.onkeyup = () => {
  //   checkPhone();
  // };

  // Check confirm password on keyup
  cpInput.onkeyup = () => {
    checkConfirmPassword();
  };

  // Check email
  function checkEmail() {
    let pattern = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;
    if (!eInput.value.match(pattern)) {
      eField.classList.add("error");
      eField.classList.remove("valid");
      let errorTxt = eField.querySelector(".error-txt");
      (eInput.value != "") ? errorTxt.innerText = "Enter a valid email address" : errorTxt.innerText = "Email can't be blank";
    } else {
      eField.classList.remove("error");
      eField.classList.add("valid");
    }
  }

  // Check password
  function checkPass() {
    if (pInput.value == "") {
      pField.classList.add("error");
      pField.classList.remove("valid");
    } else {
      pField.classList.remove("error");
      pField.classList.add("valid");
    }
  }

  // Check name
  function checkName() {
    if (nInput.value == "") {
      nField.classList.add("error");
      nField.classList.remove("valid");
    } else {
      nField.classList.remove("error");
      nField.classList.add("valid");
    }
  }

  // Check phone number
  // function checkPhone() {
  //   if (phInput.value == "") {
  //     phField.classList.add("error");
  //     phField.classList.remove("valid");
  //   } else {
  //     phField.classList.remove("error");
  //     phField.classList.add("valid");
  //   }
  // }

  // Check confirm password
  function checkConfirmPassword() {
    if (cpInput.value == "") {
      cpField.classList.add("error");
      cpField.classList.remove("valid");
    } else {
      cpField.classList.remove("error");
      cpField.classList.add("valid");
    }
  }

  // Redirect if there are no errors
  if (!eField.classList.contains("error") &&
      !pField.classList.contains("error") &&
      !nField.classList.contains("error") &&
      // !phField.classList.contains("error") &&
      !cpField.classList.contains("error")) {
    window.location.href = form.getAttribute("action");
  }


  const data = {
    name:document.getElementById('name').value,
    email:document.getElementById('email').value,
    password:document.getElementById('password').value,
    passwordConfirm:document.getElementById('confirmPassword').value
  }
  console.log(data)
  fetch("/users/signup",{
    method:"POST",
    headers: {
      "Content-Type": "application/json" 
    },
    body:JSON.stringify(data),
  })
  .then((res) => {
    if(!res.ok){
      alert(res.error)
    }
    return res.json();
  })
  .then((data) =>{
    console.log(data)
    cleareFields();
    window.location.href = 'login.html'
  })
  .catch((e) =>{
    alert(e)
  })
};

function cleareFields(){
  document.getElementById("name").innerHTML = "";
  document.getElementById("email").innerHTML = '';
  document.getElementById("password").innerHTML = '';
  document.getElementById('confirmPassword').innerHTML = '';
}