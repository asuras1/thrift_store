// const res = require("express/lib/response");

const form = document.querySelector("form");
eField = form.querySelector(".email"),
eInput = eField.querySelector("input"),
pField = form.querySelector(".password"),
pInput = pField.querySelector("input");

form.onsubmit = (e)=>{
  e.preventDefault(); //preventing from form submitting
  //if email and password is blank then add shake class in it else call specified function
  (eInput.value == "") ? eField.classList.add("shake", "error") : checkEmail();
  (pInput.value == "") ? pField.classList.add("shake", "error") : checkPass();

  setTimeout(()=>{ //remove shake class after 500ms
    eField.classList.remove("shake");
    pField.classList.remove("shake");
  }, 500);

  eInput.onkeyup = ()=>{checkEmail();} //calling checkEmail function on email input keyup
  pInput.onkeyup = ()=>{checkPass();} //calling checkPassword function on pass input keyup

  function checkEmail(){ //checkEmail function
    let pattern = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/; //pattern for validate email
    if(!eInput.value.match(pattern)){ //if pattern not matched then add error and remove valid class
      eField.classList.add("error");
      eField.classList.remove("valid");
      let errorTxt = eField.querySelector(".error-txt");
      //if email value is not empty then show please enter valid email else show Email can't be blank
      (eInput.value != "") ? errorTxt.innerText = "Enter a valid email address" : errorTxt.innerText = "Email can't be blank";
    }else{ //if pattern matched then remove error and add valid class
      eField.classList.remove("error");
      eField.classList.add("valid");
    }
  }

  function checkPass(){ //checkPass function
    if(pInput.value == ""){ //if pass is empty then add error and remove valid class
      pField.classList.add("error");
      pField.classList.remove("valid");
    }else{ //if pass is empty then remove error and add valid class
      pField.classList.remove("error");
      pField.classList.add("valid");
    }
  }

  //if eField and pField doesn't contains error class that mean user filled details properly
  if(!eField.classList.contains("error") && !pField.classList.contains("error")){
    window.location.href = form.getAttribute("action"); //redirecting user to the specified url which is inside action attribute of form tag
  }

  const data = {
    email:document.getElementById("email").value,
    password:document.getElementById("password").value,
  }
  console.log(data)
  fetch("/users/login",{
    method:"POST",
    headers:{"Content-Type":"application/json"},
    body: JSON.stringify(data),
  })
  // .then((res) =>{
  //   if(res.status === 404){
  //     alert("Invalid Login, Try again")
  //   }else if(res.status === 200){
  //     document.getElementById("email").innerHTML = '';
  //     document.getElementById("password").innerHTML = '';
  //     console.log(res);
  //     localStorage.setItem('data',JSON.stringify(res))
  //     // window.location.href = 'index.html'
  //   }else if(!res.ok){
  //     console.log(res)
  //     alert(res.error)
  //     alert("hiiii")
  //   }
    
  // })
  .then((res) => res.json())
  .then((res) =>{
    console.log(res)
    if(res.error){
      alert(res.error)
    }else{
      localStorage.setItem('data',JSON.stringify(res))
      const role = res.data.user.role;
      console.log(role)
      if(role === 'admin'){
        window.location.href = 'admin/user.html'
      }else{
        window.location.href = 'home page/index.html'
      }
    }
  })
  .catch((e) =>{
    console.log(e)
    alert(e)
  })
}
