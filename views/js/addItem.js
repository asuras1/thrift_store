
const itemForm = document.getElementById("item");

const addItem = (e) =>{
    e.preventDefault();
    const formData = new FormData();
    formData.append("itemName",document.getElementById("productName").value);
    // formData.append("itemImages",document.getElementById("images").files);
    const imageInput = document.getElementById("images");

  for (let i = 0; i < imageInput.files.length; i++) {
    formData.append("itemImages", imageInput.files[i]);
  }
    formData.append("itemDescription",document.getElementById("productDescription").value);
    formData.append("price",document.getElementById("price").value);
    
    const itemImages = document.getElementById("images").files;
    const data = JSON.parse(localStorage.getItem("data"));
    const user = data.data.user._id;
    formData.append("seller",user);
    console.log(user);
    const data2 = {
        itemName: document.getElementById("productName").value,
        itemImages: document.getElementById("images"),
        itemDescription:document.getElementById("productDescription").value,
        price:itemImages,
        seller:user
    }
    fetch("/items",{
        method:"POST",
        body:formData,
    })
    .then((res) => res.json())
    .then((res) =>{
        console.log(res)
        if(res.error){
            alert(res.error)
        }else{
            alert("Item Added Successfully")
        }
    })
}
itemForm.addEventListener("submit",addItem)