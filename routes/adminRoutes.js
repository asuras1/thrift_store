const express = require('express')
const adminController = require('./../controllers/adminController')
const authController = require('./../controllers/authController')
const itemController = require('../controllers/itemController');
const router = express.Router()

router.post('/signup', authController.signup)
router.post('/login', authController.login)

router
    .route('/')
    .get(adminController.getAllUsers)
    .post(adminController.createUser)

router
    .route('/:id')
    .get(adminController.getUser)
    .patch(itemController.upload.single('profilePhoto') ,adminController.updateAdmin)
    .delete(adminController.deleteUser)

module.exports = router

