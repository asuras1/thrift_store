const express = require('express');
const router = express.Router();
const itemController = require('../controllers/itemController');

// Create a new item
router.post('/items',itemController.upload.array('itemImages'), itemController.createItem);

// Get all items
router.get('/items', itemController.getAllItems);

// Get a single item by ID
router.get('/items/:itemId', itemController.getItemById);
router.get("/items/user/:userId",itemController.getItemByUserId);
// Update an item by ID
router.patch('/items/:itemId',itemController.upload.array('itemImages'), itemController.updateItem);

// Delete an item by ID
router.delete('/items/:itemId', itemController.deleteItem);

module.exports = router;
