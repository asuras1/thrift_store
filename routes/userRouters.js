const express = require('express')
const userController = require('./../controllers/userController')
const authController = require('./../controllers/authController')
const itemController = require('../controllers/itemController');
const {postFeedback,getAllFeedback} = require('../controllers/feedbackController');
const router = express.Router()

router.post('/signup', authController.signup)
router.post('/login', authController.login)

router
    .route('/')
    .get(userController.getAllUsers)
    .post(userController.createUser)
router.route('/fb').get(getAllFeedback);
router.route("/status/:id").patch(itemController.updateStatus);
router
    .route('/:id')
    .get(userController.getUser)
    .patch(itemController.upload.single('profilePhoto') ,userController.updateUser)
    .delete(userController.deleteUser)
router.route('/fb').post(postFeedback);

module.exports = router

