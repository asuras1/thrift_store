
const express = require("express");
const app = express();
const path = require('path')
const itemRoutes = require("./routes/itemRoutes")
const userRoutes = require("./routes/userRouters")
const adminRouter = require("./routes/adminRoutes")
app.use(express.json())
app.get("/", (req, res) => {
    console.log("hello")
    res.sendFile(path.join(__dirname, "views", "login.html"));
});
app.use(express.static('views'))
app.use(express.static('images'))

app.use("/",itemRoutes)
app.use("/users",userRoutes)
app.use("/admin",adminRouter)
module.exports = app;