
const app = require("./app");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const bodyParser = require('body-parser');
const express = require("express")

//-------------------------------------------------------------------------

//-------------------------------------------------------------------------

dotenv.config();
const port = 3000;
mongoose.connect(process.env.DATABASE).then((conn)=>{
    console.log(conn);
    console.log("connected to the database");
    app.listen(port,()=>console.log(`server is running on port ${port}`))
})