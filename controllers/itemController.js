
const Item = require('../models/itemModels');
const multer = require("multer");
const fs = require("fs");
const res = require('express/lib/response');

const multerStorage = multer.diskStorage({
  destination: (req,files,cb) =>{
    cb(null,"images")
  },
  filename:(req,file,cb) =>{
    console.log(file);
    const ext = file.mimetype.split("/")[1];
    cb(null,`item-${Date.now()}.${ext}`);
  }
});

const multerFilter = (req,file,cb) =>{
  if(file.mimetype.startsWith("image")){
    cb(null,true);
  }else{
    res.status(400).json({message:"Not an image! Please upload only images",});
  }
}
exports.upload = multer({
  storage:multerStorage,
  fileFilter:multerFilter,
});

// Create a new item
exports.createItem = async (req, res) => {
  console.log("oiiiiiiiii")
  try {
    console.log(req.body)
    const { itemName, itemDescription, price, seller } = req.body;
    console.log(itemName, itemDescription, price, seller)
    const itemImages = req.files.map((file) => file.filename);
    console.log(itemImages)
    const data = {
      itemName: itemName,
      itemImages: itemImages,
      itemDescription: itemDescription,
      price: price,
      seller: seller,
    }
    const newItem = await Item.create(data);
    res.status(201).json(newItem);
  } catch (error) {
    console.log(error)
    res.status(500).json({ error: error.message});
  }
};

// Get all items
exports.getAllItems = async (req, res) => {
  try {
    console.log(req.body)
    const items = await Item.find();
    res.status(200).json(items);
  } catch (error) {
    console.log(error)
    res.status(500).json({ error: error.message });
  }
};

// Get a single item by ID
exports.getItemById = async (req, res) => {
  try {
    const item = await Item.findById(req.params.itemId);
    if (!item) {
      return res.status(404).json({ error: 'Item not found' });
    }
    console.log(item)
    res.status(200).json({status:"success",item});
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' });
  }
};

exports.getItemByUserId = async (req,res) =>{
  try{
    console.log("hii")
    const userId = req.params.userId;
    const items = await Item.find({seller:userId})
    if(items.length === 0){
      return res.status(400).json({error:"No items created"})
    }
    console.log(items)
    res.status(200).json({status:"success",items})
  }catch(err){
    console.log(err);
    res.status(500).json({error:err.message})
  }
}
// Update an item by ID
exports.updateItem = async (req, res) => {
  try {
    console.log("hi")
    const id = req.params.itemId;
    const {itemName,price,location,itemDescription, status} = req.body;
    const itemImages = req.files.map((file) => file.filename);
    console.log(req.body)
    const data = {
      itemName,
      price,
      location,
      itemDescription,
      itemImages,
      status
    }

    const updatedItem = await Item.findByIdAndUpdate(id, {$set: data},{new:true});
    if (!updatedItem) {
      return res.status(404).json({ error: 'Item not found' });
    }
    res.status(200).json({status:"success",updatedItem});
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' });
  }
};

// Delete an item by ID
exports.deleteItem = async (req, res) => {
  try {
    const deletedItem = await Item.findByIdAndDelete(req.params.itemId);
    if (!deletedItem) {
      return res.status(404).json({ error: 'Item not found' });
    }
    res.status(204).json();
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' });
  }
};

exports.updateStatus = async (req,res) =>{
  try{
    console.log("hhhhh")
      const id = req.params.id;
      const item = await Item.findByIdAndUpdate(id,{$set:{status:"Sold"}});
      res.status(200).json({status:"success",item});
  }catch(err){
      console.log(err);
      res.status(500).json({error:err.message})
  }
}