
const User = require('../models/userModels')
const bcrypt = require("bcryptjs")
exports.getAllUsers = async (req, res, next) => {
    try {
        const users = await User.find()
        res.status(200).json({data: users, status: 'success'})
    } catch (err){
        res.status(500).json({error: err.message});
    }
}

exports.createUser = async (req, res, next) => {
    try{
        const user = await User.create(req.body);
        console.log(user)
        res.json({data: user, status: 'success'}); 
    } catch (err){
        console.log(err)

        res.status(500).json({error: err.message});
    }
}

exports.getUser = async (req, res) => {
    try{
        const user = await User.findById(req.params.id);
        res.json({data: user, status: 'success'}); 
    } catch (err){
        res.status(500).json({error: err.message});
    }
}

exports.updateAdmin = async (req, res) => {
    try{
        console.log("dasdf",req.body)
        const id = req.params.id
        const {name,oldPassword,newPassword,confirmPassword} = req.body;
        if(newPassword != confirmPassword){
            return res.status(400).json({error:"passwords don't match"})
        }
        console.log(id)
        const userE = await User.findOne({_id:id}).select('+password')
        if(!userE){
            return res.status(400).json({error:"try again"})
        }
        console.log(userE)
        console.log(oldPassword)
        if(oldPassword !== undefined){
            if(!(await userE.correctPassword(oldPassword,userE.password))){
                return res.status(400).json({error:"Old passwords do not match"});
            }
        }
        var hashP;
        if(newPassword){
            hashP = await bcrypt.hash(newPassword,12)
        }
        const profilePhoto = req.file;
        var photo;
        if(profilePhoto){
            photo = profilePhoto.filename;
        }
        // console.log(profilePhoto.filename)
        const user = await User.findByIdAndUpdate(id, {$set: {name,password:hashP,profilePhoto:photo}},{new:true});
        res.json({data: user, status: 'success'}); 
    } catch (err){
        console.log(err)
        res.status(500).json({error: err.message});
    }
}

exports.deleteUser = async (req, res) => {
    try{
        const user = await User.findByIdAndDelete(req.params.id, req.body);
        res.json({data: user, status: 'success'}); 
    } catch (err){
        res.status(500).json({error: err.message});
    }
}