const User = require('./../models/userModels')
const jwt = require('jsonwebtoken')
const dotenv = require("dotenv")
// const AppError = require('./../utils/appError')
const { use } = require('../routes/userRouters')
const { json } = require('express')
dotenv.config()
const signToken = (id) => {
    return jwt.sign({id}, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN,
    })
}

const createSendToken = (user, statusCode, res) =>{
    const token = signToken(user._id)
    const cookieOptions = {
        expiresIn:new Date(
            Date.now()+process.env.JWT_COOKIE_EXPIRES_IN *24*60*60*1000,
        ),
        httpOnly: true,
    }
    res.cookie('jwt', token, cookieOptions)
    
    res.status(statusCode).json({
        status: 'success',
        token,
        data : {
            user
        }
    })
}

exports.signup = async(req, res, next) => {
    try{
        console.log(req.body); 
          
    const newUser = await User.create(req.body)
    console.log(newUser,'new users');
    createSendToken(newUser, 201, res)
    }
    catch(err){
        console.log(err)
        res.status(500).json({error : err.message});
    }
}

exports.login = async (req, res, next)=>{
    try{
        const {email, password} = req.body
        console.log(req.body,'body')
        if(!email || !password){
            return res.status(400).json({error:"email and password are required"})
        }

        const user = await User.findOne({ email }).select('+password')
        console.log(user)
        if(!user || !(await user.correctPassword(password, user.password))){
            console.log('hello')
            return res.status(404).json({error:"invalid login, try again"})
        }
        createSendToken(user, 200, res)
    }catch(err){
        console.log(err,"ksdflsd")
        res.status(500).json({error: err.message});
    }
}